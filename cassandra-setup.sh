#!/bin/bash
# *****************************************************************************
# This is a script that will install C* and all dependencies such as Java
# Note - this may be provided as part of the training without any warranty.
#        You can use this in your own environment at your own risk
#
#
# *****************************************************************************

printf "Updating repo and installing python minimal along with other dependencies\n"
sudo apt update
sudo apt install python2-minimal -y
sudo apt install wget -y

printf "Taking ownership of /opt, creating directories, downloading C* and Java\n"
rm -rf /opt/
sudo chmod 777 /opt
mkdir /opt/softwares
cd /opt/softwares
wget https://d6opu47qoi4ee.cloudfront.net/apache-cassandra-3.11.3-bin.tar.gz
wget https://d6opu47qoi4ee.cloudfront.net/jdk-8u192-linux-x64.tar.gz

cd /opt
printf "Untaring C*\n"
tar -zxf /opt/softwares/apache-cassandra-3.11.3-bin.tar.gz
printf "Untaring Java\n"
tar -zxf /opt/softwares/jdk-8u192-linux-x64.tar.gz


printf "Modifying .profile with various paths\n"
echo '#Add the following lines at the bottom of this file' >> ~/.profile
echo 'export JAVA_HOME=/opt/jdk1.8.0_192' >> ~/.profile
echo 'export CASSANDRA_HOME=/opt/apache-cassandra-3.11.3' >> ~/.profile
echo 'PATH="$JAVA_HOME/bin:$CASSANDRA_HOME/bin:$CASSANDRA_HOME/tools/bin:$PATH"' >> ~/.profile

source ~/.profile
java -version
printf "Python version is "
python --version
printf "Cassandra location is "
which cassandra
printf "Cassandra home is $CASSANDRA_HOME\n"
printf "Java home is $JAVA_HOME\n"

cd $CASSANDRA_HOME/conf
mv cassandra.yaml cassandra.yaml.orig
mv cassandra-rackdc.properties cassandra-rackdc.properties.orig
wget https://d6opu47qoi4ee.cloudfront.net/cassandra.yaml
wget https://d6opu47qoi4ee.cloudfront.net/cassandra-rackdc.properties


CURR_DIR=$(pwd)
CLUSTER_NAME='flipbasket-cluster'
LOCAL_NODE_IP=$(hostname -I)
# printf "Provide the SEED IP address "
# read SEED_IP=$(hostname -I)
# printf "Provide the data center name "
# read DATA_CENTER
# printf "Provide the rack name "
# read RACK_NAME
SEED_IP=$(hostname -I)
DATA_CENTER=DC1
RACK_NAME=r1


printf "\nUsing the cluster name $CLUSTER_NAME"
printf "\nUsing the IP of the node as $LOCAL_NODE_IP"
printf "\nUsing the SEED IP as $SEED_IP"
printf "\nUsing the DATA CENTER as $DATA_CENTER"
printf "\nUsing the RACK as $RACK_NAME"
printf "\nCurrent directory is $CURR_DIR"

sed -i "s=MOD_CLUSTER_NAME=$CLUSTER_NAME=g" cassandra.yaml
sed -i "s=MOD_IP_ADDRESS=$LOCAL_NODE_IP=g" cassandra.yaml
sed -i "s=MOD_SEED_LIST=$SEED_IP=g" cassandra.yaml
sed -i "s=MOD_DATACENTER=$DATA_CENTER=g" cassandra-rackdc.properties
sed -i "s=MOD_RACK=$RACK_NAME=g" cassandra-rackdc.properties

sed -i 's=MOD_DATA_PATH=/opt/cassandra-data=g' cassandra.yaml
sed -i 's=MOD_COMMIT_LOG_PATH=/opt/cassandra-data=g' cassandra.yaml
sed -i 's=MOD_SAVED_CACHE_PATH=/opt/cassandra-data=g' cassandra.yaml
sed -i 's=MOD_HINTS_DIR_PATH=/opt/cassandra-data=g' cassandra.yaml
sed -i 's=MOD_CDC_PATH=/opt/cassandra-data=g' cassandra.yaml

printf "\nAll done\nSource profile file for normal ops now ....\n\n"
